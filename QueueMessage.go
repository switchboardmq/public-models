package publicmodels

import (
	"encoding/json"
	"encoding/xml"
)

// QueueMessage is a generic struct that is used in the message queue interface.
// Most often the body is marshaled into another struct.
type QueueMessage struct {
	MessageId string
	Body      string
	Ack       func()
}

func (q *QueueMessage) BodyJSON() (any, error) {
	var out struct{}
	err := json.Unmarshal([]byte(q.Body), &out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}

func (q *QueueMessage) BodyXML() (any, error) {
	var out struct{}
	err := xml.Unmarshal([]byte(q.Body), &out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}
