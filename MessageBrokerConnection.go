package publicmodels

import "github.com/google/uuid"

type DestinationType string
type SupportedBrand string
type SupportedQueueProtocol string

const (
	Queue DestinationType = "QUEUE"
	Topic                 = "TOPIC"
)

const (
	AmazonSQS SupportedBrand = "AmazonSQS"
	AmazonSNS                = "AmazonSNS"
	ActiveMQ                 = "ActiveMQ"
	RabbitMQ                 = "RabbitMQ"
)

const (
	Stomp      SupportedQueueProtocol = "STOMP"
	NoProtocol                        = "NA"
	//other options may come in the future such as AMQP1.0, Websockets, etc.
)

type MessageBrokerConnection struct {
	ID                uuid.UUID         `json:"id"`
	AccountID         uuid.UUID         `json:"accountId"`
	DestinationName   string            `json:"destinationName"`
	DestinationType   DestinationType   `json:"destinationType"`
	EventID           uuid.UUID         `json:"eventId"`
	ConnectionSetting ConnectionSetting `json:"connectionSetting"`
	ShouldSubscribe   bool              `json:"shouldSubscribe"`
}

type ConnectionSetting struct {
	ID               uuid.UUID              `json:"id"`
	Brand            SupportedBrand         `json:"brand"`
	IsSSL            bool                   `json:"isSsl"`
	Protocol         SupportedQueueProtocol `json:"protocol"`
	ConnectionString []string               `json:"connectionString"`
	Version          string                 `json:"version"`
}
