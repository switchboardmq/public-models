package publicmodels

import "github.com/google/uuid"

type EventMessagePayload struct {
	WorkflowID  uuid.UUID `json:"workflowId"`
	MessageID   uuid.UUID `json:"messageId"`
	EventID     uuid.UUID `json:"eventId"`
	ContentType string    `json:"contentType"`
	Payload     string    `json:"payload"`
}
